﻿/*
 * Copyright (c) 2015, TopCoder, Inc. All rights reserved.
 */

using System;
using log4net;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tholos.Services;
using Tholos.Services.Impl;

namespace Tholos.Tests.services
{
    /// <summary>
    /// Unit tests for <see cref="AuthenticationService" /> class.
    /// </summary>
    /// 
    /// <author>
    /// veshu
    /// </author>
    ///
    /// <version>
    /// 1.0
    /// </version>
    ///
    /// <copyright>
    /// Copyright (c) 2015, TopCoder, Inc. All rights reserved.
    /// </copyright>
    [TestClass]
    public class AuthenticationServiceUnitTest
    {
        /// <summary>
        /// Represents the service instance used for testing methods.
        /// </summary>
        private readonly AuthenticationService _instance = new AuthenticationService();

        /// <summary>
        /// Sets up the environment before executing each test in this class.
        /// </summary>
        [TestInitialize]
        public void SetUp()
        {
            TestHelper.FillDatabase();
            _instance.ConnectionStringName = TestHelper.ConnectionStringName;
            _instance.DatabaseTypeName = TestHelper.DatabaseTypeName;
            _instance.Logger = LogManager.GetLogger("Test");

        }

        /// <summary>
        /// Cleans up the environment after executing each test in this class.
        /// </summary>
        [TestCleanup]
        public virtual void CleanUp()
        {
            TestHelper.ClearDatabase();
        }


        /// <summary>
        /// Accuracy test of the class constructor.
        /// </summary>
        [TestMethod]
        public void TestCtorAccuracy()
        {
            Assert.AreEqual(typeof(BasePersistenceService), _instance.GetType().BaseType,
                "The class should inherit from BasePersistenceService.");
            Assert.IsTrue(_instance is IAuthenticationService,
                "The class should implement IAuthenticationService.");
        }

        #region CheckConfiguration() method tests

        /// <summary>
        /// Accuracy test of <c>CheckConfiguration</c> method,
        /// should not throw exceptions.
        /// </summary>
        [TestMethod]
        public void TestCheckConfigurationAccuracy()
        {
            // act
            _instance.CheckConfiguration();
        }

        /// <summary>
        /// Failure test of <c>CheckConfiguration</c> method,
        /// should throw <see cref="ConfigurationException"/> when <c>ConnectionStringName</c> is <c>null</c>.
        /// </summary>
        [TestMethod, ExpectedException(typeof(ConfigurationException))]
        public void TestCheckConfigurationConnectionStringNull()
        {
            // arrange
            _instance.ConnectionStringName = null;

            // act
            _instance.CheckConfiguration();
        }

        /// <summary>
        /// Failure test of <c>CheckConfiguration</c> method,
        /// should throw <see cref="ConfigurationException"/> when <c>ConnectionStringName</c> is empty.
        /// </summary>
        [TestMethod, ExpectedException(typeof(ConfigurationException))]
        public void TestCheckConfigurationConnectionStringEmpty()
        {
            // arrange
            _instance.ConnectionStringName = TestHelper.EmptyString;

            // act
            _instance.CheckConfiguration();
        }

        /// <summary>
        /// Failure test of <c>CheckConfiguration</c> method,
        /// should throw <see cref="ConfigurationException"/> when <c>Logger</c> is <c>null</c>.
        /// </summary>
        [TestMethod, ExpectedException(typeof(ConfigurationException))]
        public void TestCheckConfigurationLoggerNull()
        {
            // arrange
            _instance.Logger = null;

            // act
            _instance.CheckConfiguration();
        }

        #endregion

        #region Authenticate

        /// <summary>
        /// Accuracy test of <c>Authenticate</c> method,
        /// result should be correct.
        /// </summary>
        [TestMethod]
        public void TestAuthenticateManagerAccuracy()
        {
            // Found for user
            var authenticated = _instance.Authenticate("topcoder", "password");
            Assert.IsTrue(authenticated);

        }
        /// <summary>
        /// Accuracy test of <c>Authenticate</c> method,
        /// result should be correct.
        /// </summary>
        [TestMethod]
        public void TestAuthenticateParticipantAccuracy()
        {
            // Found for participant
            var authenticated = _instance.Authenticate("participant1", "CdDc4");
            Assert.IsTrue(authenticated);


        }

        /// <summary>
        /// Accuracy test of <c>Authenticate</c> method,
        /// result should be correct.
        /// </summary>
        [TestMethod]
        public void TestAuthenticateFalseAccuracy()
        {
            // Not found
            var authenticated = _instance.Authenticate("notexist", "wrongpassword");
            Assert.IsFalse(authenticated);
        }

        /// <summary>
        /// Failure tests of <c>Authenticate</c> method.
        /// </summary>
        [TestMethod]
        public void TestAuthenticateFailure()
        {
            // username is null
            TestHelper.AssertThrows<ArgumentNullException>(() =>
                _instance.Authenticate(null, "password"));

            // username is empty
            TestHelper.AssertThrows<ArgumentException>(() =>
                _instance.Authenticate(TestHelper.EmptyString, "password"));

            // password is null
            TestHelper.AssertThrows<ArgumentNullException>(() =>
                _instance.Authenticate("topcoder", null));

            // password is empty
            TestHelper.AssertThrows<ArgumentException>(() =>
                _instance.Authenticate("topcoder", TestHelper.EmptyString));
        }

        #endregion
    }
}
