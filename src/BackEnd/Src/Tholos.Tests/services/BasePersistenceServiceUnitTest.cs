﻿/*
 * Copyright (c) 2015, TopCoder, Inc. All rights reserved.
 */

using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tholos.Services;
using Tholos.Services.Impl;

namespace Tholos.Tests.services
{
    /// <summary>
    /// Represents base class for testing services that are inherited from <see cref="BasePersistenceService"/>.
    /// </summary>
    ///
    /// <typeparam name="TService">The actual type of the service being tested.</typeparam>
    ///
    /// <author>
    /// veshu
    /// </author>
    ///
    /// <version>
    /// 1.0
    /// </version>
    ///
    /// <copyright>
    /// Copyright (c) 2015, TopCoder, Inc. All rights reserved.
    /// </copyright>
    public abstract class BasePersistenceServiceUnitTest<TService> : BaseServiceUnitTest<TService>
        where TService : BasePersistenceService, new()
    {
        /// <summary>
        /// Sets up the environment before executing each test in this class.
        /// </summary>
        [TestInitialize]
        public override void SetUp()
        {
            base.SetUp();
            TestHelper.FillDatabase();
            _instance.ConnectionStringName = TestHelper.ConnectionStringName;
            _instance.DatabaseTypeName = TestHelper.DatabaseTypeName;
        }

        /// <summary>
        /// Cleans up the environment after executing each test in this class.
        /// </summary>
        [TestCleanup]
        public virtual void CleanUp()
        {
            TestHelper.ClearDatabase();
        }

        #region CheckConfiguration() method tests

        /// <summary>
        /// Failure test of <c>CheckConfiguration</c> method,
        /// should throw <see cref="ConfigurationException"/> when <c>ConnectionStringName</c> is <c>null</c>.
        /// </summary>
        [TestMethod, ExpectedException(typeof(ConfigurationException))]
        public void TestCheckConfigurationConnectionStringNull()
        {
            // arrange
            _instance.ConnectionStringName = null;

            // act
            _instance.CheckConfiguration();
        }

        /// <summary>
        /// Failure test of <c>CheckConfiguration</c> method,
        /// should throw <see cref="ConfigurationException"/> when <c>ConnectionStringName</c> is empty.
        /// </summary>
        [TestMethod, ExpectedException(typeof(ConfigurationException))]
        public void TestCheckConfigurationConnectionStringEmpty()
        {
            // arrange
            _instance.ConnectionStringName = TestHelper.EmptyString;

            // act
            _instance.CheckConfiguration();
        }


        /// <summary>
        /// Failure test of <c>CheckConfiguration</c> method,
        /// should throw <see cref="ConfigurationException"/> when <c>DatabaseTypeName</c> is <c>null</c>.
        /// </summary>
        [TestMethod, ExpectedException(typeof(ConfigurationException))]
        public void TestCheckDatabaseTypeNameStringNull()
        {
            // arrange
            _instance.DatabaseTypeName = null;

            // act
            _instance.CheckConfiguration();
        }

        /// <summary>
        /// Failure test of <c>CheckConfiguration</c> method,
        /// should throw <see cref="ConfigurationException"/> when <c>DatabaseTypeName</c> is empty.
        /// </summary>
        [TestMethod, ExpectedException(typeof(ConfigurationException))]
        public void TestCheckDatabaseTypeNameStringEmpty()
        {
            // arrange
            _instance.DatabaseTypeName = TestHelper.EmptyString;

            // act
            _instance.CheckConfiguration();
        }

        #endregion
    }
}
