﻿/*
 * Copyright (c) 2015, TopCoder, Inc. All rights reserved.
 */

using log4net;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tholos.Services;
using Tholos.Services.Impl;

namespace Tholos.Tests.services
{
    /// <summary>
    /// Represents base class for testing services that are inherited from <see cref="BaseService"/>.
    /// </summary>
    ///
    /// <typeparam name="TService">The actual type of the service being tested.</typeparam>
    ///
    /// <author>
    /// veshu
    /// </author>
    ///
    /// <version>
    /// 1.0
    /// </version>
    ///
    /// <copyright>
    /// Copyright (c) 2015, TopCoder, Inc. All rights reserved.
    /// </copyright>
    public abstract class BaseServiceUnitTest<TService> where TService : BaseService, new()
    {
        /// <summary>
        /// Represents the service instance used for tests.
        /// </summary>
        protected TService _instance = new TService();

        /// <summary>
        /// Sets up the environment before executing each test in this class.
        /// </summary>
        [TestInitialize]
        public virtual void SetUp()
        {
            _instance.Logger = LogManager.GetLogger("Test");
        }

        #region CheckConfiguration() method tests

        /// <summary>
        /// Accuracy test of <c>CheckConfiguration</c> method,
        /// should not throw exceptions.
        /// </summary>
        [TestMethod]
        public void TestCheckConfigurationAccuracy()
        {
            // act
            _instance.CheckConfiguration();
        }

        /// <summary>
        /// Failure test of <c>CheckConfiguration</c> method,
        /// should throw <see cref="ConfigurationException"/> when <c>Logger</c> is <c>null</c>.
        /// </summary>
        [TestMethod, ExpectedException(typeof(ConfigurationException))]
        public void TestCheckConfigurationLoggerNull()
        {
            // arrange
            _instance.Logger = null;

            // act
            _instance.CheckConfiguration();
        }

        #endregion
    }
}
