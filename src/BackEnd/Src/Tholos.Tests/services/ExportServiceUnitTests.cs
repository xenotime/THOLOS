﻿/*
 * Copyright (c) 2015, TopCoder, Inc. All rights reserved.
 */

using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.ServiceModel.Web;
using CsvHelper;
using log4net;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tholos.Entities;
using Tholos.Services;
using Tholos.Services.Impl;

namespace Tholos.Tests.services
{
    /// <summary>
    /// Unit tests for <see cref="ExportService" /> class.
    /// </summary>
    /// 
    /// <author>
    /// veshu
    /// </author>
    ///
    /// <version>
    /// 1.0
    /// </version>
    ///
    /// <copyright>
    /// Copyright (c) 2015, TopCoder, Inc. All rights reserved.
    /// </copyright>
    [TestClass]
    public class ExportServiceUnitTests
    {
        /// <summary>
        /// Represents the <see cref="ExportService"/> instance used for tests.
        /// </summary>
        private readonly ExportService _instance = new ExportService();

        /// <summary>
        /// Sets up the environment before executing each test in this class.
        /// </summary>
        [TestInitialize]
        public void SetUp()
        {
            _instance.Logger = LogManager.GetLogger("Test");
        }

        /// <summary>
        /// Cleans up the environment after executing each test in this class.
        /// </summary>
        [TestCleanup]
        public void CleanUp()
        {
            TestHelper.ClearDatabase();
        }

        /// <summary>
        /// Accuracy test of the class constructor.
        /// </summary>
        [TestMethod]
        public void TestCtorAccuracy()
        {
            Assert.AreEqual(typeof(BaseService), _instance.GetType().BaseType,
                "The class should inherit from BaseService.");
            Assert.IsTrue(_instance != null,
                "The class should implement IExportService.");
        }

        #region CheckConfiguration() method tests

        /// <summary>
        /// Accuracy test of <c>CheckConfiguration</c> method,
        /// should not throw exceptions.
        /// </summary>
        [TestMethod]
        public void TestCheckConfigurationAccuracy()
        {
            // act
            _instance.CheckConfiguration();
        }

        /// <summary>
        /// Failure test of <c>CheckConfiguration</c> method,
        /// should throw <see cref="ConfigurationException"/> when <c>Logger</c> is <c>null</c>.
        /// </summary>
        [TestMethod, ExpectedException(typeof(ConfigurationException))]
        public void TestCheckConfigurationLoggerNull()
        {
            // arrange
            _instance.Logger = null;

            // act
            _instance.CheckConfiguration();
        }
        #endregion

        #region ExportSurveyResults method tests
        /// <summary>
        /// Tests the accuracy of ExportSurveyResults() method
        /// Success
        /// </summary>
        [TestMethod]
        public void TestExportSurveyResultsAccuracy()
        {
            var participantSurveys = new List<ParticipantSurvey>
            {
                new ParticipantSurvey
                {
                    SurveyId = 1,
                    ParticipantId = 1,
                    Id = 1,
                    CurrentPrototypeTestId = 1
                },
                new ParticipantSurvey
                {
                    SurveyId = 1,
                    ParticipantId = 2,
                    Id = 2,
                    CurrentPrototypeTestId = 2
                },
                new ParticipantSurvey
                {
                    SurveyId = 2,
                    ParticipantId = 1,
                    Id = 3,
                    CurrentPrototypeTestId = 1
                },

            };

            var bytes = _instance.ExportSurveyResults(participantSurveys);

            Assert.IsNotNull(bytes, "export is wrong");
            var stream = new MemoryStream(bytes);
            var fileName = Path.GetTempFileName();
            var outputResult = new List<ParticipantSurvey>();

            try
            {
                using (FileStream fs = File.OpenWrite(fileName))
                {
                    stream.CopyTo(fs);
                }
                using (TextReader txtReader = File.OpenText(fileName))
                {
                    CsvReader csvReader = new CsvReader(txtReader);
                    csvReader.Configuration.HasHeaderRecord = false;
                    while (csvReader.Read())
                    {
                        outputResult.Add(new ParticipantSurvey
                        {
                            Id = csvReader.GetField<long>(0),
                            SurveyId = csvReader.GetField<long>(1),
                            ParticipantId = csvReader.GetField<long>(2),
                            CurrentPrototypeTestId = csvReader.GetField<long>(3)

                        });

                    }
                }
            }
            finally
            {
                File.Delete(fileName);
            }
            Assert.AreEqual(participantSurveys.Count, outputResult.Count, "output record count is wrong.");
            for (int i = 0; i < outputResult.Count; i++)
            {
                TestHelper.AreEqual(participantSurveys[i], outputResult[i]);
            }
        }

        /// <summary>
        /// Tests the accuracy of ExportSurveyResults() method
        /// Success with empty
        /// </summary>
        [TestMethod]
        public void TestExportSurveyResultsAccuracy1()
        {
            var participantSurveys = new List<ParticipantSurvey>();

            var bytes = _instance.ExportSurveyResults(participantSurveys);

            Assert.IsNotNull(bytes, "export is wrong");
            var stream = new MemoryStream(bytes);
            var fileName = Path.GetTempFileName();
            var outputResult = new List<ParticipantSurvey>();

            try
            {
                using (FileStream fs = File.OpenWrite(fileName))
                {
                    stream.CopyTo(fs);
                }
                using (TextReader txtReader = File.OpenText(fileName))
                {
                    CsvReader csvReader = new CsvReader(txtReader);
                    csvReader.Configuration.HasHeaderRecord = false;
                    while (csvReader.Read())
                    {
                        outputResult.Add(new ParticipantSurvey
                        {
                            Id = csvReader.GetField<long>(0),
                            SurveyId = csvReader.GetField<long>(1),
                            ParticipantId = csvReader.GetField<long>(2),
                            CurrentPrototypeTestId = csvReader.GetField<long>(3)

                        });

                    }
                }
            }
            finally
            {
                File.Delete(fileName);
            }
            Assert.AreEqual(participantSurveys.Count, outputResult.Count, "output record count is wrong.");
        }
        /// <summary>
        /// Tests the accuracy of ExportSurveyResults() method
        /// failure  with null
        /// </summary>
        [TestMethod, ExpectedException(typeof(WebFaultException<ServiceFaultDetail>))]
        public void TestExportSurveyResultsAccuracy2()
        {
            _instance.ExportSurveyResults(null);
        }

        #endregion

        #region ExportProductImages method tests

        /// <summary>
        /// Tests the accuracy of ExportProductImages() method
        /// Success
        /// </summary>
        [TestMethod]
        public void TestExportProductImagesAccuracy()
        {
            var participantSurveys = new List<ParticipantSurvey>
            {
                new ParticipantSurvey
                {
                    SurveyId = 1,
                    ParticipantId = 1,
                    Id = 1,
                    CurrentPrototypeTestId = 1,
                    PrototypeTests = new List<PrototypeTest>
                    {
                        new PrototypeTest
                        {
                            Id = 1,
                            BeforePhotoLocalPath = Path.GetFullPath("../../test_files/download.jpg"),
                            AfterPhotoLocalPath = Path.GetFullPath("../../test_files/meow.jpg")
                        }
                    }
                },
                new ParticipantSurvey
                {
                    SurveyId = 1,
                    ParticipantId = 2,
                    Id = 2,
                    CurrentPrototypeTestId = 2,
                    PrototypeTests = new List<PrototypeTest>
                    {
                        new PrototypeTest
                        {
                            Id = 1,
                            BeforePhotoLocalPath = Path.GetFullPath("../../test_files/download1.jpg"),
                            AfterPhotoLocalPath = Path.GetFullPath("../../test_files/meow1.jpg")
                        }
                    }
                },
                new ParticipantSurvey
                {
                    SurveyId = 2,
                    ParticipantId = 1,
                    Id = 3,
                    CurrentPrototypeTestId = 1,
                    PrototypeTests = new List<PrototypeTest>
                    {
                        new PrototypeTest
                        {
                            Id = 1,
                            BeforePhotoLocalPath = Path.GetFullPath("../../test_files/download2.jpg"),
                            AfterPhotoUri = null
                        }
                    }
                },

            };
            var bytes = _instance.ExportProductImages(participantSurveys);

            // verify
            Assert.IsNotNull(bytes, "export is wrong");
            var stream = new MemoryStream(bytes);
            var fileName = Path.GetTempFileName();
            var outputResult = new List<ParticipantSurvey>();

            try
            {
                using (FileStream fs = File.OpenWrite(fileName))
                {
                    stream.CopyTo(fs);
                }
                using (ZipArchive archive = ZipFile.OpenRead(fileName))
                {
                    var csvFile = archive.GetEntry("PrototypeTests.csv");
                    Assert.IsNotNull(csvFile);
                    using (TextReader txtReader = new StreamReader(csvFile.Open()))
                    {
                        CsvReader csvReader = new CsvReader(txtReader);
                        csvReader.Configuration.HasHeaderRecord = false;
                        while (csvReader.Read())
                        {
                            outputResult.Add(new ParticipantSurvey
                            {
                                Id = csvReader.GetField<long>(0),
                                SurveyId = csvReader.GetField<long>(1),
                            });
                            var beforeFileName = csvReader.GetField<string>(2);
                            var afterPhotoName = csvReader.GetField<string>(3);
                            Assert.IsNotNull(beforeFileName);
                            Assert.IsTrue(File.Exists(Path.Combine("../../test_files/", beforeFileName)));
                            if (!string.IsNullOrEmpty(afterPhotoName))
                            {
                                Assert.IsTrue(File.Exists(Path.Combine("../../test_files/", afterPhotoName)));
                            }

                        }
                    }
                }
            }
            finally
            {
                File.Delete(fileName);
            }
            Assert.AreEqual(participantSurveys.Count, outputResult.Count, "output record count is wrong.");

        }

        /// <summary>
        /// Tests the accuracy of ExportProductImages() method
        /// failure  with null
        /// </summary>
        [TestMethod, ExpectedException(typeof(WebFaultException<ServiceFaultDetail>))]
        public void TestExportProductImagesAccuracy2()
        {
            _instance.ExportProductImages(null);
        }

        #endregion

        #region ExportParticipantCredentials method tests
        /// <summary>
        /// Tests the accuracy of ExportParticipantCredentials() method
        /// Success
        /// </summary>
        [TestMethod]
        public void TestExportParticipantCredentialsAccuracy()
        {
            var credentials = new List<ParticipantCredentials>
            {
                new ParticipantCredentials
                {
                    Id = 1,
                    SurveyId = 1,
                    Username = "username1",
                    Password = "password",
                    Used = true
                },
                new ParticipantCredentials
                {
                    Id = 2,
                    SurveyId = 1,
                    Username = "username2",
                    Password = "password",
                    Used = true
                },
                new ParticipantCredentials
                {
                    Id = 3,
                    SurveyId = 2,
                    Username = "username2",
                    Password = "password",
                    Used = false
                },
            };

            var bytes = _instance.ExportParticipantCredentials(credentials);

            Assert.IsNotNull(bytes, "export is wrong");
            var stream = new MemoryStream(bytes);
            var fileName = Path.GetTempFileName();
            var outputResult = new List<ParticipantCredentials>();

            try
            {
                using (FileStream fs = File.OpenWrite(fileName))
                {
                    stream.CopyTo(fs);
                }
                using (TextReader txtReader = File.OpenText(fileName))
                {
                    CsvReader csvReader = new CsvReader(txtReader);
                    csvReader.Configuration.HasHeaderRecord = false;
                    while (csvReader.Read())
                    {
                        outputResult.Add(new ParticipantCredentials
                        {
                            Id = csvReader.GetField<long>(0),
                            SurveyId = csvReader.GetField<long>(1),
                            Username = csvReader.GetField<string>(2),
                            Password = csvReader.GetField<string>(3),
                            Used = csvReader.GetField<bool>(4)
                        });
                    }
                }
            }
            finally
            {
                File.Delete(fileName);
            }
            Assert.AreEqual(credentials.Count, outputResult.Count, "output record count is wrong.");
            for (int i = 0; i < outputResult.Count; i++)
            {
                TestHelper.AreEqual(credentials[i], outputResult[i]);
            }
        }

        /// <summary>
        /// Tests the accuracy of ExportParticipantCredentials() method
        /// Success with empty
        /// </summary>
        [TestMethod]
        public void TestExportParticipantCredentialsAccuracy1()
        {
            var credentials = new List<ParticipantCredentials>();

            var bytes = _instance.ExportParticipantCredentials(credentials);

            Assert.IsNotNull(bytes, "export is wrong");
            var stream = new MemoryStream(bytes);
            var fileName = Path.GetTempFileName();
            var outputResult = new List<ParticipantCredentials>();

            try
            {
                using (FileStream fs = File.OpenWrite(fileName))
                {
                    stream.CopyTo(fs);
                }
                using (TextReader txtReader = File.OpenText(fileName))
                {
                    CsvReader csvReader = new CsvReader(txtReader);
                    csvReader.Configuration.HasHeaderRecord = false;
                    while (csvReader.Read())
                    {
                        outputResult.Add(new ParticipantCredentials
                        {
                            Id = csvReader.GetField<long>(0),
                            SurveyId = csvReader.GetField<long>(1),
                            Username = csvReader.GetField<string>(2),
                            Password = csvReader.GetField<string>(3),
                            Used = csvReader.GetField<bool>(4)
                        });
                    }
                }
            }
            finally
            {
                File.Delete(fileName);
            }
            Assert.AreEqual(credentials.Count, outputResult.Count, "output record count is wrong.");
        }

        /// <summary>
        /// Tests the accuracy of ExportParticipantCredentials() method
        /// failure  with null
        /// </summary>
        [TestMethod, ExpectedException(typeof(WebFaultException<ServiceFaultDetail>))]
        public void TestExportParticipantCredentialsAccuracy2()
        {
            _instance.ExportParticipantCredentials(null);
        }

        #endregion
    }
}
