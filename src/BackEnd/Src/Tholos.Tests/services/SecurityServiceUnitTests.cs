﻿/*
 * Copyright (c) 2015, TopCoder, Inc. All rights reserved.
 */

using System;
using log4net;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tholos.Services;
using Tholos.Services.Impl;

namespace Tholos.Tests.services
{
    /// <summary>
    /// <para>
    /// Unit tests for <see cref="SecurityService"/> class.
    /// </para>
    /// <para>
    /// Changes in version 1.1 (THOLOS - Integrate Prototype with Backend APIs):
    /// <list type="bullet">
    /// <item>
    /// <description>Fixed the test for user role.</description>
    /// </item>
    /// </list>
    /// </para>
    /// </summary>
    ///
    /// <author>veshu</author>
    /// <author>duxiaoyang</author>
    ///
    /// <version>1.1</version>
    /// <since>1.0</since>
    ///
    /// <copyright>
    /// Copyright (c) 2015, TopCoder, Inc. All rights reserved.
    /// </copyright>
    [TestClass]
    public class SecurityServiceUnitTests
    {
        /// <summary>
        /// Represents the <see cref="SecurityService"/> instance used for tests.
        /// </summary>
        private readonly SecurityService _instance = new SecurityService();

        /// <summary>
        /// Sets up the environment before executing each test in this class.
        /// </summary>
        [TestInitialize]
        public void SetUp()
        {
            TestHelper.FillDatabase();
            _instance.ConnectionStringName = TestHelper.ConnectionStringName;
            _instance.DatabaseTypeName = TestHelper.DatabaseTypeName;
            _instance.Logger = LogManager.GetLogger("Test");

        }

        /// <summary>
        /// Cleans up the environment after executing each test in this class.
        /// </summary>
        [TestCleanup]
        public virtual void CleanUp()
        {
            TestHelper.ClearDatabase();
        }

        /// <summary>
        /// Accuracy test of the class constructor.
        /// </summary>
        [TestMethod]
        public void TestCtorAccuracy()
        {
            Assert.AreEqual(typeof(BasePersistenceService), _instance.GetType().BaseType,
                "The class should inherit from BasePersistenceService.");
            Assert.IsTrue(_instance is ISecurityService,
                "The class should implement ISecurityService.");
        }

        #region CheckConfiguration() method tests

        /// <summary>
        /// Accuracy test of <c>CheckConfiguration</c> method,
        /// should not throw exceptions.
        /// </summary>
        [TestMethod]
        public void TestCheckConfigurationAccuracy()
        {
            // act
            _instance.CheckConfiguration();
        }

        /// <summary>
        /// Failure test of <c>CheckConfiguration</c> method,
        /// should throw <see cref="ConfigurationException"/> when <c>ConnectionStringName</c> is <c>null</c>.
        /// </summary>
        [TestMethod, ExpectedException(typeof(ConfigurationException))]
        public void TestCheckConfigurationConnectionStringNull()
        {
            // arrange
            _instance.ConnectionStringName = null;

            // act
            _instance.CheckConfiguration();
        }

        /// <summary>
        /// Failure test of <c>CheckConfiguration</c> method,
        /// should throw <see cref="ConfigurationException"/> when <c>ConnectionStringName</c> is empty.
        /// </summary>
        [TestMethod, ExpectedException(typeof(ConfigurationException))]
        public void TestCheckConfigurationConnectionStringEmpty()
        {
            // arrange
            _instance.ConnectionStringName = TestHelper.EmptyString;

            // act
            _instance.CheckConfiguration();
        }

        /// <summary>
        /// Failure test of <c>CheckConfiguration</c> method,
        /// should throw <see cref="ConfigurationException"/> when <c>Logger</c> is <c>null</c>.
        /// </summary>
        [TestMethod, ExpectedException(typeof(ConfigurationException))]
        public void TestCheckConfigurationLoggerNull()
        {
            // arrange
            _instance.Logger = null;

            // act
            _instance.CheckConfiguration();
        }
        #endregion

        /// <summary>
        /// Accuracy test of <c>GetUser</c> method.
        /// </summary>
        [TestMethod]
        public void TestGetUserAccuracy1()
        {
            // arrange
            string username = "topcoder";

            // act
            var roles = _instance.GetRoles(username);

            // assert
            Assert.IsNotNull(roles, "roles is expected to be not null.");
            Assert.AreEqual(1, roles.Count, "roles count is wrong.");
            Assert.AreEqual("Manager", roles[0], "roles is wrong.");
        }

        /// <summary>
        /// Accuracy test of <c>GetUser</c> method.
        /// </summary>
        [TestMethod]
        public void TestGetUserAccuracyWithParticipant()
        {
            // arrange
            string username = "participant1";

            // act
            var roles = _instance.GetRoles(username);

            // assert
            Assert.IsNotNull(roles, "roles is expected to be not null.");
            Assert.AreEqual(1, roles.Count, "roles count is wrong.");
            Assert.AreEqual("Participant", roles[0], "roles is wrong.");
        }

        /// <summary>
        /// Accuracy test of <c>GetUser</c> method
        /// </summary>
        [TestMethod]
        public void TestGetUserAccuracy2()
        {
            // arrange
            string username = "NotExistUserName";

            // act
            var result = _instance.GetRoles(username);

            // assert
            Assert.IsNotNull(result, "result is expected to be null.");
            Assert.AreEqual(0, result.Count, "result is wrong.");
        }

        /// <summary>
        /// Accuracy test of <c>GetUser</c> method with empty username.
        /// </summary>
        [TestMethod]
        public void TestGetUserAccuracy3()
        {
            // arrange
            string username = "";
            TestHelper.AssertThrows<ArgumentException>(() => _instance.GetRoles(username));
        }

        /// <summary>
        /// Accuracy test of <c>GetUser</c> method with null username.
        /// </summary>
        [TestMethod]
        public void TestGetUserAccuracy4()
        {
            // arrange
            string username = null;
            TestHelper.AssertThrows<ArgumentNullException>(() => _instance.GetRoles(username));
        }
    }
}
