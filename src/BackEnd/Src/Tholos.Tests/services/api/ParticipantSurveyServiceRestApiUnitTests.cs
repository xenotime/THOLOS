﻿/*
 * Copyright (c) 2015, TopCoder, Inc. All rights reserved.
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.ServiceModel.Web;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Tholos.Entities;
using Tholos.Services;
using Tholos.Services.Impl;
using Tholos.Tests.services.api.proxy;

namespace Tholos.Tests.services.api
{
    /// <summary>
    /// <para>
    /// Unit tests for <see cref="IParticipantSurveyService"/> class.
    /// </para>
    /// <para>
    /// Changes in version 1.1 (THOLOS - Integrate Prototype with Backend APIs):
    /// <list type="bullet">
    /// <item>
    /// <description>Changed to send request using participant account.</description>
    /// </item>
    /// </list>
    /// </para>
    /// </summary>
    ///
    /// <author>veshu</author>
    /// <author>duxiaoyang</author>
    ///
    /// <version>1.1</version>
    /// <since>1.0</since>
    ///
    /// <copyright>
    /// Copyright (c) 2015, TopCoder, Inc. All rights reserved.
    /// </copyright>
    [TestClass]
    public class ParticipantSurveyServiceRestApiUnitTests : BasePersistenceServiceUnitTest<ParticipantSurveyService>
    {
        /// <summary>
        /// The service host address.
        /// </summary>
        private static readonly string ServiceHostAddress =
            TestHelper.WcfServiceHostBaseUri + typeof(ParticipantSurveyService).Name + ".svc/";


        /// <summary>
        /// Represents the <see cref="ParticipantSurveyServiceRestApiProxy"/> instance used for REST API tests.
        /// </summary>
        private readonly IParticipantSurveyService _restApiProxy = new ParticipantSurveyServiceRestApiProxy();

        /// <summary>
        /// Sets up the environment before executing each test in this class.
        /// </summary>
        [TestInitialize]
        public override void SetUp()
        {
            base.SetUp();
            TestHelper.FillDatabase();
            _instance.BaseFileUri = TestHelper.BaseFileUri;
            _instance.FileUploadPath = TestHelper.FileUploadPath;
            _instance.ProductPhotoFilenameTemplate = TestHelper.ProductPhotoFilenameTemplate;
        }

        /// <summary>
        /// Accuracy test of the class constructor.
        /// </summary>
        [TestMethod]
        public void TestCtorAccuracy()
        {
            Assert.AreEqual(typeof(BasePersistenceService), _instance.GetType().BaseType,
                "The class should inherit from BasePersistenceService.");
            Assert.IsTrue(_instance is IParticipantSurveyService,
                "The class should implement IParticipantSurveyService.");
        }

        #region CheckConfiguration() method tests

        /// <summary>
        /// Failure test of <c>CheckConfiguration</c> method,
        /// should throw <see cref="ConfigurationException"/> when <c>FileUploadPath</c> is <c>null</c>.
        /// </summary>
        [TestMethod, ExpectedException(typeof(ConfigurationException))]
        public void TestCheckFileUploadPathNull()
        {
            // arrange
            _instance.FileUploadPath = null;

            // act
            _instance.CheckConfiguration();
        }

        /// <summary>
        /// Failure test of <c>CheckConfiguration</c> method,
        /// should throw <see cref="ConfigurationException"/> when <c>BaseFileUri</c> is <c>null</c>.
        /// </summary>
        [TestMethod, ExpectedException(typeof(ConfigurationException))]
        public void TestCheckBaseFileUriNull()
        {
            // arrange
            _instance.BaseFileUri = null;

            // act
            _instance.CheckConfiguration();
        }
        /// <summary>
        /// Failure test of <c>CheckConfiguration</c> method,
        /// should throw <see cref="ConfigurationException"/> when <c>ProductPhotoFilenameTemplate</c> is <c>null</c>.
        /// </summary>
        [TestMethod, ExpectedException(typeof(ConfigurationException))]
        public void TestCheckProductPhotoFilenameTemplateNull()
        {
            // arrange
            _instance.BaseFileUri = null;

            // act
            _instance.CheckConfiguration();
        }

        /// <summary>
        /// Success test of <c>CheckConfiguration</c> method,
        /// should not throw any exception.
        /// </summary>
        [TestMethod]
        public void TestCheckConfiguration()
        {
            // act
            _instance.CheckConfiguration();
        }

        #endregion

        #region UploadPhoto() method tests

        /// <summary>
        /// Tests the accuracy of UploadPhoto() method.
        /// Covers success scenario.
        /// </summary>
        [TestMethod]
        public void TestUploadPhotoAccuracy()
        {
            const string filename = "download.jpg";
            var bytes = File.ReadAllBytes("../../test_files/meow.jpg");
            var newTempFileName = "";
            var resp = UploadFile(bytes, "/ParticipantSurveys/UploadPhoto?filename=" + filename);
            var fullFilePath = "";
            try
            {
                if (resp != null)
                {
                    newTempFileName = JsonConvert.DeserializeObject<string>(resp);
                }

                Assert.IsTrue(newTempFileName.Length > 1, "upload method is wrong");

                fullFilePath = Path.Combine(Path.GetFullPath("../../../Tholos.Host/uploadFiles/"),
                    newTempFileName);

                Assert.IsTrue(File.Exists(fullFilePath));
            }
            finally
            {
                if (File.Exists(fullFilePath))
                {
                    File.Delete(fullFilePath);
                }
            }
        }

        /// <summary>
        /// Tests the accuracy of UploadPhoto() method.
        /// null stream
        /// </summary>
        [TestMethod]
        public void TestUploadPhotoAccuracy1()
        {
            const string filename = "";
            var bytes = File.ReadAllBytes("../../test_files/meow.jpg");
            TestHelper.AssertThrows<ArgumentException>(
                () => UploadFile(bytes, "/ParticipantSurveys/UploadPhoto?filename=" + filename));
        }

        #endregion

        #region GetParticipantSurvey() method tests
        /// <summary>
        /// Tests the GetParticipantSurvey by participant survey id.
        /// </summary>
        [TestMethod]
        public void TestGetParticipantSurveyAccuracy()
        {
            var id = "1";

            var result = _restApiProxy.GetParticipantSurvey(id);

            Assert.IsNotNull(result, "participant  can not be null");
            Assert.AreEqual(0, result.PostTestingAnswers.Count, "Post testing answer count is wrong.");
            Assert.AreEqual(1, result.PrototypeTests.Count, "Prototype tests count is wrong.");
            Assert.AreEqual(1, result.Id, "Id is wrong");
        }

        /// <summary>
        /// Accuracy test of <c>GetParticipantSurvey</c> method .
        /// </summary>
        [TestMethod]
        public void TestGetParticipantSurvey2()
        {
            // arrange
            string id = "invalidNumber";

            TestHelper.AssertThrows<ArgumentException>(() => _restApiProxy.GetParticipantSurvey(id));
        }

        /// <summary>
        /// Accuracy test of <c>GetParticipantSurvey</c> method.
        ///  negatvie user id.
        /// </summary>
        [TestMethod]
        public void TestGetParticipantSurvey3()
        {
            // arrange
            string id = "-10";
            TestHelper.AssertThrows<ArgumentException>(() => _restApiProxy.GetParticipantSurvey(id));
        }

        #endregion

        #region CreatePrototypeTest() method tests
        /// <summary>
        /// Tests the CreatePrototypeTest method
        /// </summary>
        [TestMethod]
        public void TestCreatePrototypeTest()
        {
            long participantSurveyId = 1;
            var prototypeTest = new PrototypeTest
            {
                Iteration = 1,
                BeforeWeight = 33,
                PrototypeCode = "code2"
            };
            const string filename = "download.jpg";
            var bytes = File.ReadAllBytes("../../test_files/meow.jpg");
            var newTempFileName = "";
            var resp = UploadFile(bytes, "/ParticipantSurveys/UploadPhoto?filename=" + filename);
            var fullFilePath = "";
            long newId = 0;
            try
            {
                if (resp != null)
                {
                    newTempFileName = JsonConvert.DeserializeObject<string>(resp);
                }

                Assert.IsTrue(newTempFileName.Length > 1, "upload method is wrong");

                fullFilePath = Path.Combine(Path.GetFullPath("../../../Tholos.Host/uploadFiles/"),
                    newTempFileName);

                Assert.IsTrue(File.Exists(fullFilePath));

                newId = _restApiProxy.CreatePrototypeTest(participantSurveyId.ToString(), prototypeTest,
                    newTempFileName);

            }
            finally
            {
                if (File.Exists(fullFilePath))
                {
                    File.Delete(fullFilePath);
                }
            }
            Assert.IsTrue(newId > 0, "result is wrong");
            TestHelper.AssertDatabaseRecordCount("PrototypeTest", new Dictionary<string, object>
                {
                    {"PrototypeCode", prototypeTest.PrototypeCode},
                    {"ParticipantSurveyId", participantSurveyId},
                    {"Iteration", prototypeTest.Iteration},
                    {"Id", newId}
                }, 1);
        }
        /// <summary>
        /// Tests the CreatePrototypeTest method
        /// empty filename
        /// </summary>
        [TestMethod]
        public void TestCreatePrototypeTest1()
        {
            long participantSurveyId = 1;
            var prototypeTest = new PrototypeTest
            {
                Iteration = 1,
                BeforeWeight = 33,
                PrototypeCode = "code2"
            };
            TestHelper.AssertThrows<ArgumentException>(
                () => _restApiProxy.CreatePrototypeTest(participantSurveyId.ToString(), prototypeTest,
                    ""));
        }


        /// <summary>
        /// Tests the CreatePrototypeTest method
        /// negative survey id
        /// </summary>
        [TestMethod]
        public void TestCreatePrototypeTest2()
        {
            string participantSurveyId = "-1";
            var prototypeTest = new PrototypeTest
            {
                Iteration = 1,
                BeforeWeight = 33,
                PrototypeCode = "code2"
            };
            TestHelper.AssertThrows<ArgumentException>(
                () => _restApiProxy.CreatePrototypeTest(participantSurveyId, prototypeTest,
                    ""));
        }

        /// <summary>
        /// Tests the CreatePrototypeTest method
        /// survey not found
        /// </summary>
        [TestMethod]
        public void TestCreatePrototypeTest3()
        {
            string participantSurveyId = "12";
            var prototypeTest = new PrototypeTest
            {
                Iteration = 1,
                BeforeWeight = 33,
                PrototypeCode = "code2"
            };
            TestHelper.AssertThrows<EntityNotFoundException>(
                () => _restApiProxy.CreatePrototypeTest(participantSurveyId, prototypeTest,
                    "test.jpg"));
        }

        /// <summary>
        /// Tests the CreatePrototypeTest method
        /// null prototype test
        /// </summary>
        [TestMethod]
        public void TestCreatePrototypeTest4()
        {
            string participantSurveyId = "1";

            TestHelper.AssertThrows<ArgumentNullException>(
                () => _restApiProxy.CreatePrototypeTest(participantSurveyId, null,
                    ""));
        }
        #endregion

        #region GetPrototypeTest() method tests
        /// <summary>
        /// Accuracy test of <c>GetPrototypeTest</c> method,
        /// no exception is expected to be thrown.
        /// </summary>
        [TestMethod]
        public void TestGetPrototypeTestAccuracy1()
        {
            var result = _restApiProxy.GetPrototypeTest("1");

            Assert.AreEqual(1, result.Id, "Id is wrong");
            Assert.AreEqual("code1", result.PrototypeCode, "result.PrototypeCode is wrong");
            Assert.AreEqual(13, Math.Ceiling(result.BeforeWeight), "esult.BeforeWeight is wrong");

        }

        /// <summary>
        /// Accuracy test of <c>GetPrototypeTest</c> method,
        /// </summary>
        [TestMethod]
        public void TestGetPrototypeTestAccuracy2()
        {
            // arrange
            string id = "6";

            // act
            var result = _restApiProxy.GetPrototypeTest(id);

            // assert
            Assert.IsNull(result, "result is expected to be null.");
        }

        /// <summary>
        /// Accuracy test of <c>GetPrototypeTest</c> method .
        /// </summary>
        [TestMethod]
        public void TestGetPrototypeTestAccuracy3()
        {
            // arrange
            string id = "invalidNumber";
            TestHelper.AssertThrows<ArgumentException>(() => _restApiProxy.GetPrototypeTest(id));
        }

        /// <summary>
        /// Accuracy test of <c>GetPrototypeTest</c> method.
        /// </summary>
        [TestMethod]
        public void TestGetPrototypeTestAccuracy5()
        {
            // arrange
            string id = "-10";
            TestHelper.AssertThrows<ArgumentException>(() => _restApiProxy.GetPrototypeTest(id));
        }
        #endregion

        #region SubmitPrototypeAnswer() method tests
        /// <summary>
        /// Accuracy test of <c>SubmitPrototypeAnswer</c> method.
        /// </summary>
        [TestMethod]
        public void TestSubmitPrototypeAnswer1()
        {

            // arrange
            string id = "1";
            RangeAnswer answer = new RangeAnswer
            {
                Value = 2,
                AnswerType = AnswerType.RangeAnswer,
                QuestionId = 1
            };
            TestHelper.AssertDatabaseRecordCount("PrototypeAnswer", new Dictionary<string, object>
            {
                {"QuestionId", answer.QuestionId},
                {"AnswerType", answer.AnswerType.ToString()},
                {"PrototypeTestId", id}
            }, 0);
            // act
            _restApiProxy.SubmitPrototypeAnswer(id, answer);

            //verify
            // after complete count should be increased
            TestHelper.AssertDatabaseRecordCount("PrototypeAnswer", new Dictionary<string, object>
            {
                {"QuestionId", answer.QuestionId},
                {"AnswerType", answer.AnswerType.ToString()},
                {"PrototypeTestId", id}
            }, 1);

        }

        /// <summary>
        /// Accuracy test of <c>SubmitPrototypeAnswer</c> method,
        /// answr null
        /// </summary>
        [TestMethod]
        public void TestSubmitPrototypeAnswer2()
        {
            // arrange
            string id = "1";
            SingleAnswer answer = null;
            //verify
            TestHelper.AssertThrows<ArgumentNullException>(() => _restApiProxy.SubmitPrototypeAnswer(id, answer));

        }

        /// <summary>
        /// Accuracy test of <c>SubmitPrototypeAnswer</c> method,
        /// not found 
        /// </summary>
        [TestMethod]
        public void TestSubmitPrototypeAnswer5()
        {
            // arrange
            string id = "111";
            var answer = new SingleAnswer();
            //verify
            TestHelper.AssertThrows<EntityNotFoundException>(() => _restApiProxy.SubmitPrototypeAnswer(id, answer));

        }

        /// <summary>
        /// Accuracy test of <c>SubmitPrototypeAnswer</c> method .
        /// </summary>
        [TestMethod]
        public void TestSubmitPrototypeAnswer3()
        {
            // arrange
            string id = "invalidNumber";
            var answer = new SingleAnswer();

            TestHelper.AssertThrows<ArgumentException>(() => _restApiProxy.SubmitPrototypeAnswer(id, answer));
        }

        /// <summary>
        /// Accuracy test of <c>SubmitPrototypeAnswer</c> method.
        ///  negatvie user id.
        /// </summary>
        [TestMethod]
        public void TestSubmitPrototypeAnswer4()
        {
            // arrange
            string id = "-10";
            var answer = new SingleAnswer();

            TestHelper.AssertThrows<ArgumentException>(() => _restApiProxy.SubmitPrototypeAnswer(id, answer));
        }
        #endregion

        #region UpdatePrototypeAnswer() method tests

        /// <summary>
        /// Tests the UpdatePrototypeAnswer accuracy.
        /// success update
        /// </summary>
        [TestMethod]
        public void TestUpdatePrototypeAnswer()
        {
            var answer = new SingleAnswer
            {
                Id = 1,
                AnswerOptionId = 2,
                AnswerType = AnswerType.SingleAnswer,
                QuestionId = 1
            };
            TestHelper.AssertDatabaseRecordCount("PrototypeSingleAnswer", new Dictionary<string, object>
            {
                {"Id", answer.Id},
                {"AnswerOptionId", answer.AnswerOptionId}
            }, 0);
            _restApiProxy.UpdatePrototypeAnswer(answer);

            TestHelper.AssertDatabaseRecordCount("PrototypeSingleAnswer", new Dictionary<string, object>
            {
                {"Id", answer.Id},
               {"AnswerOptionId", answer.AnswerOptionId}
            }, 1);

        }
        /// <summary>
        /// Tests the UpdatePrototypeAnswer accuracy.
        /// null answer
        /// </summary>
        [TestMethod]
        public void TestUpdatePrototypeAnswer1()
        {
            TestHelper.AssertThrows<ArgumentNullException>(() => _restApiProxy.UpdatePrototypeAnswer(null));
        }

        /// <summary>
        /// Tests the UpdatePrototypeAnswer accuracy.
        /// not existing answer
        /// </summary>
        [TestMethod]
        public void TestUpdatePrototypeAnswer2()
        {
            var answer = new SingleAnswer
            {
                Id = 13,
                AnswerOptionId = 1,
                AnswerType = AnswerType.SingleAnswer,
                QuestionId = 1
            };
            TestHelper.AssertThrows<EntityNotFoundException>(() => _restApiProxy.UpdatePrototypeAnswer(answer));
        }


        #endregion

        #region CompletePrototypeTest() method tests
        /// <summary>
        /// Tests the CompletePrototypeTest with success case
        /// </summary>
        [TestMethod]
        public void TestCompletePrototypeTest()
        {
            var id = "1";
            var afterWeight = 10;
            var bytes = File.ReadAllBytes("../../test_files/meow.jpg");
            var fileName = "meow.jpg";
            TestHelper.AssertDatabaseRecordCount("PrototypeTest", new Dictionary<string, object>
            {
                {"Id", id},
                {"Completed", true}
            }, 0);


            CompletePrototypeTest(id, fileName, afterWeight, bytes);
            var newFileName = "11code11_after.jpg";
            var fullFilePath = "";
            TestHelper.AssertDatabaseRecordCount("PrototypeTest", new Dictionary<string, object>
            {
                {"Id", id},
                {"Completed", true}
            }, 1);

            try
            {
                fullFilePath = Path.Combine(Path.GetFullPath("../../../Tholos.Host/uploadFiles/"),
                        newFileName);

                Assert.IsTrue(File.Exists(fullFilePath));
            }
            finally
            {
                if (File.Exists(fullFilePath))
                {
                    File.Delete(fullFilePath);
                }
            }

        }


        /// <summary>
        /// Tests the CompletePrototypeTest with invalid file name.
        /// </summary>
        [TestMethod]
        public void TestCompletePrototypeTest1()
        {
            var id = "1";
            var afterWeight = 10;
            var bytes = File.ReadAllBytes("../../test_files/meow.jpg");
            var fileName = "meowad";

            TestHelper.AssertThrows<ArgumentException>(
                () => CompletePrototypeTest(id, fileName, afterWeight, bytes));
        }

        /// <summary>
        /// Tests the CompletePrototypeTest with invalid  prototype test id.
        /// </summary>
        [TestMethod]
        public void TestCompletePrototypeTest2()
        {
            var id = "-1";
            var afterWeight = 10;
            var bytes = File.ReadAllBytes("../../test_files/meow.jpg");
            var fileName = "meow.jpg";

            TestHelper.AssertThrows<ArgumentException>(
                () => CompletePrototypeTest(id, fileName, afterWeight, bytes));
        }

        /// <summary>
        /// Tests the CompletePrototypeTest with invalid prototype test id not found.
        /// </summary>
        [TestMethod]
        public void TestCompletePrototypeTest3()
        {
            var id = "12";
            var afterWeight = 10;
            var bytes = File.ReadAllBytes("../../test_files/meow.jpg");
            var fileName = "meow.jpg";

            TestHelper.AssertThrows<EntityNotFoundException>(
                () => CompletePrototypeTest(id, fileName, afterWeight, bytes));
        }
        #endregion

        #region SubmitPostTestingAnswer() method tests
        /// <summary>
        /// Accuracy test of <c>SubmitPostTestingAnswer</c> method.
        /// </summary>
        [TestMethod]
        public void TestSubmitPostTestingAnswer1()
        {
            // arrange
            string id = "1";
            SingleAnswer answer = new SingleAnswer
            {
                AnswerOptionId = 1,
                AnswerType = AnswerType.SingleAnswer,
                QuestionId = 1
            };
            TestHelper.AssertDatabaseRecordCount("PostTestingAnswer", new Dictionary<string, object>
            {
                {"QuestionId", 1},
                {"AnswerType", AnswerType.SingleAnswer.ToString()},
                {"ParticipantSurveyId", id}
            }, 0);
            // act
            _restApiProxy.SubmitPostTestingAnswer(id, answer);

            //verify
            // after complete count should be increased
            TestHelper.AssertDatabaseRecordCount("PostTestingAnswer", new Dictionary<string, object>
            {
                {"QuestionId", 1},
                {"AnswerType", AnswerType.SingleAnswer.ToString()},
                {"ParticipantSurveyId", id}
            }, 1);

        }

        /// <summary>
        /// Accuracy test of <c>SubmitPostTestingAnswer</c> method,
        /// answr null
        /// </summary>
        [TestMethod]
        public void TestSubmitPostTestingAnswer2()
        {
            // arrange
            string id = "1";
            SingleAnswer answer = null;
            //verify
            TestHelper.AssertThrows<ArgumentNullException>(() => _restApiProxy.SubmitPostTestingAnswer(id, answer));

        }

        /// <summary>
        /// Accuracy test of <c>SubmitPostTestingAnswer</c> method,
        /// not found 
        /// </summary>
        [TestMethod]
        public void TestSubmitPostTestingAnswer5()
        {
            // arrange
            string id = "111";
            var answer = new SingleAnswer();
            //verify
            TestHelper.AssertThrows<EntityNotFoundException>(() => _restApiProxy.SubmitPostTestingAnswer(id, answer));

        }

        /// <summary>
        /// Accuracy test of <c>SubmitPostTestingAnswer</c> method .
        /// </summary>
        [TestMethod]
        public void TestSubmitPostTestingAnswer3()
        {
            // arrange
            string id = "invalidNumber";
            var answer = new SingleAnswer();

            TestHelper.AssertThrows<ArgumentException>(() => _restApiProxy.SubmitPostTestingAnswer(id, answer));
        }

        /// <summary>
        /// Accuracy test of <c>SubmitPostTestingAnswer</c> method.
        ///  negatvie user id.
        /// </summary>
        [TestMethod]
        public void TestSubmitPostTestingAnswer4()
        {
            // arrange
            string id = "-10";
            var answer = new SingleAnswer();

            TestHelper.AssertThrows<ArgumentException>(() => _restApiProxy.SubmitPostTestingAnswer(id, answer));
        }
        #endregion

        #region CompletePostTesting() method tests
        /// <summary>
        /// Accuracy test of <c>CompletePostTesting</c> method.
        /// </summary>
        [TestMethod]
        public void TestCompletePostTesting7()
        {
            // arrange
            string id = "1";
            TestHelper.AssertDatabaseRecordCount("Survey", new Dictionary<string, object>
            {
                {"id", 1},
                {"CompletedSurveysNumber", 1}
            }, 0);
            // act
            _restApiProxy.CompletePostTesting(id);

            //verify
            // after complete count should be increased
            TestHelper.AssertDatabaseRecordCount("Survey", new Dictionary<string, object>
            {
                {"id", 1},
                {"CompletedSurveysNumber", 1}
            }, 1);
        }

        /// <summary>
        /// Accuracy test of <c>CompletePostTesting</c> method,
        /// not found 
        /// </summary>
        [TestMethod]
        public void TestCompletePostTesting5()
        {
            // arrange
            string id = "111";

            //verify
            TestHelper.AssertThrows<EntityNotFoundException>(() => _restApiProxy.CompletePostTesting(id));

        }

        /// <summary>
        /// Accuracy test of <c>CompletePostTesting</c> method .
        /// </summary>
        [TestMethod]
        public void TestCompletePostTesting3()
        {
            // arrange
            string id = "invalidNumber";

            TestHelper.AssertThrows<ArgumentException>(() => _restApiProxy.CompletePostTesting(id));
        }

        /// <summary>
        /// Accuracy test of <c>CompletePostTesting</c> method.
        ///  negatvie user id.
        /// </summary>
        [TestMethod]
        public void TestCompletePostTesting4()
        {
            // arrange
            string id = "-10";
            TestHelper.AssertThrows<ArgumentException>(() => _restApiProxy.CompletePostTesting(id));
        }
        #endregion

        #region GetPrototypeQuestions() method tests
        /// <summary>
        /// Accuracy test of <c>GetPrototypeQuestions</c> method.
        /// </summary>
        [TestMethod]
        public void TestGetPrototypeQuestions7()
        {

            // arrange
            string id = "1";
            // act
            var result = _restApiProxy.GetPrototypeQuestions(id);

            //verify
            Assert.IsNotNull(result, "result must not be null");
            Assert.AreEqual(3, result.Count, "result.Count is wrong");
            TestHelper.VerifyAnswerOption(result);
        }

        /// <summary>
        /// Accuracy test of <c>GetPrototypeQuestions</c> method,
        /// empty result
        /// </summary>
        [TestMethod]
        public void TestGetPrototypeQuestions5()
        {
            // arrange
            string id = "111";
            // act
            var result = _restApiProxy.GetPrototypeQuestions(id);

            //verify
            Assert.IsNotNull(result, "result must not be null");
            Assert.AreEqual(0, result.Count, "result.Count is wrong");

        }

        /// <summary>
        /// Accuracy test of <c>GetPrototypeQuestions</c> method .
        /// </summary>
        [TestMethod]
        public void TestGetPrototypeQuestions3()
        {
            // arrange
            string id = "invalidNumber";

            TestHelper.AssertThrows<ArgumentException>(() => _restApiProxy.GetPrototypeQuestions(id));
        }

        /// <summary>
        /// Accuracy test of <c>GetPrototypeQuestions</c> method.
        ///  negatvie user id.
        /// </summary>
        [TestMethod]
        public void TestGetPrototypeQuestions4()
        {
            // arrange
            string id = "-10";
            TestHelper.AssertThrows<ArgumentException>(() => _restApiProxy.GetPrototypeQuestions(id));
        }
        #endregion

        #region GetPostTestingQuestions() method tests

        /// <summary>
        /// Accuracy test of <c>GetPostTestingQuestions</c> method.
        /// </summary>
        [TestMethod]
        public void TestGetPostTestingQuestions7()
        {

            // arrange
            string id = "1";
            // act
            var result = _restApiProxy.GetPostTestingQuestions(id);

            //verify
            Assert.IsNotNull(result, "result must not be null");
            Assert.AreEqual(3, result.Count, "result.Count is wrong");
            TestHelper.VerifyAnswerOption(result);
        }

        /// <summary>
        /// Accuracy test of <c>GetPostTestingQuestions</c> method,
        /// empty result
        /// </summary>
        [TestMethod]
        public void TestGetPostTestingQuestions5()
        {
            // arrange
            string id = "111";
            // act
            var result = _restApiProxy.GetPostTestingQuestions(id);

            //verify
            Assert.IsNotNull(result, "result must not be null");
            Assert.AreEqual(0, result.Count, "result.Count is wrong");

        }

        /// <summary>
        /// Accuracy test of <c>GetPostTestingQuestions</c> method .
        /// </summary>
        [TestMethod]
        public void TestGetPostTestingQuestions3()
        {
            // arrange
            string id = "invalidNumber";

            TestHelper.AssertThrows<ArgumentException>(() => _restApiProxy.GetPostTestingQuestions(id));
        }

        /// <summary>
        /// Accuracy test of <c>GetPostTestingQuestions</c> method.
        ///  negatvie user id.
        /// </summary>
        [TestMethod]
        public void TestGetPostTestingQuestions4()
        {
            // arrange
            string id = "-10";
            TestHelper.AssertThrows<ArgumentException>(() => _restApiProxy.GetPostTestingQuestions(id));
        }
        #endregion

        #region GetSurveyPrototypeCodes() method tests
        /// <summary>
        /// Accuracy test of <c>GetSurveyPrototypeCodes</c> method.
        /// verify update case
        /// </summary>
        [TestMethod]
        public void TestGetSurveyPrototypeCodes1()
        {
            // arrange
            string id = "1";
            // act
            var result = _restApiProxy.GetSurveyPrototypeCodes(id);

            //verify
            Assert.IsNotNull(result, "codes must not be null");
            Assert.AreEqual(3, result.Count, "result.Count is wrong");
            Assert.AreEqual("Code1", result[0], "result.Count is wrong");
            Assert.AreEqual("Code2", result[1], "result.Count is wrong");
            Assert.AreEqual("Code3", result[2], "result.Count is wrong");

        }

        /// <summary>
        /// Accuracy test of <c>GetSurveyPrototypeCodes</c> method .
        /// </summary>
        [TestMethod]
        public void TestGetSurveyPrototypeCodes3()
        {
            // arrange
            string id = "invalidNumber";

            TestHelper.AssertThrows<ArgumentException>(() => _restApiProxy.GetSurveyPrototypeCodes(id));
        }

        /// <summary>
        /// Accuracy test of <c>GetSurveyPrototypeCodes</c> method.
        ///  negatvie user id.
        /// </summary>
        [TestMethod]
        public void TestGetSurveyPrototypeCodes4()
        {
            // arrange
            string id = "-10";
            TestHelper.AssertThrows<ArgumentException>(() => _restApiProxy.GetSurveyPrototypeCodes(id));
        }
        #endregion

        /// <summary>
        /// uploads the photo.
        /// </summary>
        /// <param name="bytes">The byte[] of photo.</param>
        /// <param name="uri">The uri to post</param>
        /// <returns>The response string.</returns>
        private string UploadFile(byte[] bytes, string uri, string username = "participant1")
        {
            var client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes(username + ":CdDc4");
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic",
                Convert.ToBase64String(byteArray));
            var content = new StreamContent(new MemoryStream(bytes));
            content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
            content.Headers.ContentLength = bytes.Length;
            HttpResponseMessage response = client.PostAsync(ServiceHostAddress + uri, content).Result;
            string responseString = response.Content.ReadAsStringAsync().Result;
            if (response.StatusCode != HttpStatusCode.OK)
            {
                ServiceFaultDetail faultDetail =
                    JsonConvert.DeserializeObject<ServiceFaultDetail>(responseString);
                throw new WebFaultException<ServiceFaultDetail>(faultDetail, response.StatusCode);
            }
            return responseString;
        }

        /// <summary>
        /// Posts the request to complete the prototype test
        /// </summary>
        /// <param name="id">the prototype test id.</param>
        /// <param name="fileName">The filename.</param>
        /// <param name="afterWeight">The after weight.</param>
        /// <param name="bytes">The bytes[] of photo.</param>
        private static void CompletePrototypeTest(string id, string fileName, int afterWeight, byte[] bytes)
        {

            var uri = "PrototypeTests/" + id + "/Complete?fileName=" + fileName +
                      "&afterWeight=" + afterWeight;
            var client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes("participant1:CdDc4");
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic",
                Convert.ToBase64String(byteArray));
            var content = new StreamContent(new MemoryStream(bytes));
            content.Headers.ContentLength = bytes.Length;
            content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
            HttpResponseMessage response = client.PutAsync(ServiceHostAddress + uri, content).Result;
            string responseString = response.Content.ReadAsStringAsync().Result;
            if (response.StatusCode != HttpStatusCode.OK)
            {
                ServiceFaultDetail faultDetail =
                    JsonConvert.DeserializeObject<ServiceFaultDetail>(responseString);
                throw new WebFaultException<ServiceFaultDetail>(faultDetail, response.StatusCode);
            }
            response.EnsureSuccessStatusCode();
        }
    }
}
