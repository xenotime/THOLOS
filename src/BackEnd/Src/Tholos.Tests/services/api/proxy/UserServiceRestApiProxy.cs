﻿/*
 * Copyright (c) 2015, TopCoder, Inc. All rights reserved.
 */

using Tholos.Entities;
using Tholos.Services;
using Tholos.Services.Impl;

namespace Tholos.Tests.services.api.proxy
{
    /// <summary>
    /// Represents proxy class for hosted WCF RESTful <see cref="UserService"/> service.
    /// </summary>
    ///
    /// <author>
    /// veshu
    /// </author>
    ///
    /// <version>
    /// 1.0
    /// </version>
    ///
    /// <copyright>
    /// Copyright (c) 2015, TopCoder, Inc. All rights reserved.
    /// </copyright>
    public class UserServiceRestApiProxy : RestApiProxyBase<UserService>, IUserService
    {
        /// <summary>
        /// Gets the user by username.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <returns>The user.</returns>
        public User GetUser(string username)
        {
            if (username != null)
            {
                return GetResponse<User>("/Users/Params?username=" + username, "GET");
            }
            else
            {
                return GetResponse<User>("/Users/Params", "GET");
            }
        }
    }
}
