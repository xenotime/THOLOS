/*
* Copyright (c) 2015, TopCoder, Inc. All rights reserved.
*/

using System.Runtime.Serialization;

namespace Tholos.Entities
{
    /// <summary>
    /// This represents answer type.
    /// </summary>
    /// 
    /// <remarks>
    /// <para>
    /// This enum is marked with the <see cref="DataContractAttribute"/> attribute
    /// and the properties with the <see cref="EnumMemberAttribute"/> attribute.
    /// </para>
    /// </remarks>
    ///
    /// <threadsafety>
    /// The enum are always thread-safe.
    /// </threadsafety>
    ///
    /// <author>veshu</author>
    ///
    /// <version>1.0</version>
    /// <copyright>Copyright (c) 2015, TopCoder, Inc. All rights reserved.</copyright>
    [DataContract]
    public enum AnswerType
    {
        /// <summary>
        /// Represents single answer.
        /// </summary>
        [EnumMember]
        SingleAnswer,

        /// <summary>
        /// Represents range answer.
        /// </summary>
        [EnumMember]
        RangeAnswer,

        /// <summary>
        /// Represents single answer input.
        /// </summary>
        [EnumMember]
        SingleAnswerInput
    }
}

