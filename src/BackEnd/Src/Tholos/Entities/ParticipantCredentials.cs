/*
* Copyright (c) 2015, TopCoder, Inc. All rights reserved.
*/

using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Tholos.Entities
{

    /// <summary>
    /// This represents participant credentials.
    /// </summary>
    /// 
    /// <remarks>
    /// <para>
    /// This class is marked with the <see cref="DataContractAttribute"/> attribute
    /// and the properties with the <see cref="DataMemberAttribute"/> attribute.
    /// </para>
    /// </remarks>
    ///
    /// <threadsafety>
    /// This class is mutable, so it is not thread-safe.
    /// </threadsafety>
    ///
    /// <author>veshu</author>
    ///
    /// <version>1.0</version>
    /// <copyright>Copyright (c) 2015, TopCoder, Inc. All rights reserved.</copyright>
    [DataContract]
    public class ParticipantCredentials : IdentifiableEntity
    {
        /// <summary>
        ///  Gets or sets survey id.
        /// </summary>
        /// <value>The survey id</value>
        [DataMember]
        [Required]
        public long SurveyId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets  username.
        /// </summary>
        /// <value>The username</value>
        [DataMember]
        [Required]
        [MaxLength(64)]
        public string Username
        {
            get;
            set;
        }

        /// <summary>
        ///  Gets or sets  password.
        /// </summary>
        /// <value>The password.</value>
        [DataMember]
        [Required]
        [MaxLength(64)]
        public string Password
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the Used flag.
        /// </summary>
        /// <value>The Used flag</value>
        [DataMember]
        public bool Used { get; set; }

        /// <summary>
        /// <para>
        /// Initializes new instance of the <see cref="ParticipantCredentials"/> class.
        /// </para>
        /// </summary>
        public ParticipantCredentials()
        {
        }
    }
}

