/*
* Copyright (c) 2015, TopCoder, Inc. All rights reserved.
*/

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Tholos.Entities
{

    /// <summary>
    /// This represents participant survey.
    /// </summary>
    /// 
    /// <remarks>
    /// <para>
    /// This class is marked with the <see cref="DataContractAttribute"/> attribute
    /// and the properties with the <see cref="DataMemberAttribute"/> attribute.
    /// </para>
    /// </remarks>
    ///
    /// <threadsafety>
    /// This class is mutable, so it is not thread-safe.
    /// </threadsafety>
    ///
    /// <author>veshu</author>
    ///
    /// <version>1.0</version>
    /// <copyright>Copyright (c) 2015, TopCoder, Inc. All rights reserved.</copyright>
    [DataContract]
    public class ParticipantSurvey : IdentifiableEntity
    {
        /// <summary>
        /// Gets or sets survey id.
        /// </summary>
        /// <value>The survey id.</value>
        [DataMember]
        [Required]
        public long SurveyId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets participant id.
        /// </summary>
        /// <value>The participant id.</value>
        [DataMember]
        [Required]
        public long ParticipantId
        {
            get;
            set;
        }
        /// <summary>
        /// Gets or sets prototype tests.
        /// </summary>
        /// <value>The prototype tests.</value>
        [DataMember]
        public IList<PrototypeTest> PrototypeTests
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets current prototype test id.
        /// </summary>
        /// <value>The current prototype test id.</value>
        [DataMember]
        [Required]
        public long CurrentPrototypeTestId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets post testing answer.
        /// </summary>
        /// <value>The post testing answer.</value>
        [DataMember]
        public IList<Answer> PostTestingAnswers
        {
            get;
            set;
        }

        /// <summary>
        /// <para>
        /// Initializes new instance of the <see cref="ParticipantSurvey"/> class.
        /// </para>
        /// </summary>
        public ParticipantSurvey()
        {
        }
    }
}

