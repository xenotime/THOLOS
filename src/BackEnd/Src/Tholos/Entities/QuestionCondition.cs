/*
* Copyright (c) 2015, TopCoder, Inc. All rights reserved.
*/

using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Tholos.Entities
{
    /// <summary>
    /// This represents question condition.
    /// </summary>
    /// 
    /// <remarks>
    /// <para>
    /// This class is marked with the <see cref="DataContractAttribute"/> attribute
    /// and the properties with the <see cref="DataMemberAttribute"/> attribute.
    /// </para>
    /// </remarks>
    ///
    /// <threadsafety>
    /// This class is mutable, so it is not thread-safe.
    /// </threadsafety>
    ///
    /// <author>veshu</author>
    ///
    /// <version>1.0</version>
    /// <copyright>Copyright (c) 2015, TopCoder, Inc. All rights reserved.</copyright>
    [DataContract]
    public class QuestionCondition : IdentifiableEntity
    {
        /// <summary>
        /// Gets or sets conditional on question id.
        /// </summary>
        /// <value>The conditional on question id.</value>
        [DataMember]
        [Required]
        public long ConditionalOnQuestionId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets conditional on answer id.
        /// </summary>
        /// <value>The conditional on answer id.</value>
        [DataMember]
        [Required]
        public long ConditionalOnAnswerId
        {
            get;
            set;
        }

        /// <summary>
        /// <para>
        /// Initializes new instance of the <see cref="QuestionCondition"/> class.
        /// </para>
        /// </summary>
        public QuestionCondition()
        {
        }
    }
}

