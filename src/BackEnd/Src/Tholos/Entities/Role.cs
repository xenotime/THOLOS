/*
* Copyright (c) 2015, TopCoder, Inc. All rights reserved.
*/

using System.Runtime.Serialization;

namespace Tholos.Entities
{
    /// <summary>
    /// This represents role.
    /// </summary>
    /// 
    /// <remarks>
    /// <para>
    /// This class is marked with the <see cref="DataContractAttribute"/> attribute
    /// and the properties with the <see cref="DataMemberAttribute"/> attribute.
    /// </para>
    /// </remarks>
    ///
    /// <threadsafety>
    /// This class is mutable, so it is not thread-safe.
    /// </threadsafety>
    ///
    /// <author>veshu</author>
    ///
    /// <version>1.0</version>
    /// <copyright>Copyright (c) 2015, TopCoder, Inc. All rights reserved.</copyright>
    [DataContract]
    public class Role : NamedEntity
    {

        /// <summary>
        /// <para>
        /// Initializes new instance of the <see cref="Role"/> class.
        /// </para>
        /// </summary>
        public Role()
        {
        }
    }
}

