/*
*  Copyright (c) 2015, TopCoder, Inc. All rights reserved.
*/

using System;
using System.IdentityModel.Claims;
using System.IdentityModel.Policy;
using System.Security;
using System.Security.Principal;
using System.ServiceModel.Web;
using log4net;
using Microsoft.Practices.Unity;
using Tholos.Services;

namespace Tholos.Security
{
    /// <summary>
    /// <para>
    /// This is class implements the IAuthorizationPolicy that uses CustomPrincipal class.
    /// </para>
    /// </summary>
    ///
    /// <threadsafety>
    /// This class is immutable and thread-safe.
    /// </threadsafety>
    ///
    /// <author>veshu</author>
    /// 
    /// <version>1.0</version>
    /// <copyright>Copyright (c) 2015, TopCoder, Inc. All rights reserved.</copyright>
    public class AuthorizationPolicy : IAuthorizationPolicy
    {
        /// <summary>
        /// Represents the unique identifier that identifies this authorization component.
        /// </summary>
        /// <remark>
        /// <para>
        /// It should not be null.
        /// </para>
        /// </remark>
        public string Id
        {
            get
            {
                return Guid.NewGuid().ToString();
            }
        }

        /// <summary>
        /// Represents the claim set that represents the issuer of the authorization policy.
        /// </summary>
        /// <remark>
        /// <para>
        /// It should not be null.
        /// </para>
        /// </remark>
        public ClaimSet Issuer
        {
            get
            {
                return ClaimSet.System;

            }
        }

        /// <summary>
        /// <para>
        /// Gets or sets the security service.
        /// </para>
        /// </summary>
        /// <value>
        /// The security service instance. It should not be null after initialization through Unity injection. 
        /// </value>
        [Dependency]
        public ISecurityService SecurityService
        {
            set;
            get;
        }

        /// <summary>
        /// <para>
        /// Gets or sets the logger. 
        /// </para>
        /// </summary>
        /// <value>
        /// The logger. It should not be null after initialization through Unity injection. 
        /// </value>
        [Dependency]
        public ILog Logger
        {
            get;
            set;
        }

        /// <summary>
        /// <para>
        /// Initializes a new instance of the <see cref="AuthorizationPolicy"/> class.
        /// </para>
        /// </summary>
        public AuthorizationPolicy()
        {
        }

        /// <summary>
        /// <para>
        /// Evaluates whether a user meets the requirements for this authorization policy.
        /// </para>
        /// </summary>
        /// <param name="evaluationContext">
        /// The EvaluationContext that contains the claim set that the authorization policy evaluates.
        /// </param>
        /// <param name="state">The custom state for this authorization policy. </param>
        /// <returns><c>true</c> to state no additional evaluation is required by 
        /// this authorization policy.</returns>
        /// 
        /// <exception cref="SecurityException">If unable to evaluate the evaluation context.</exception>
        /// <exception cref="TholosException">If any other errors occur while performing this operation</exception>
        public bool Evaluate(EvaluationContext evaluationContext, ref object state)
        {
            return Helper.LoggingWrapper(Logger, () =>
            {
                if (WebOperationContext.Current != null)
                {
                    var username = WebOperationContext.Current.IncomingRequest.Headers["X-UserName"];

                    // create the authenticated client identity
                    IIdentity client = new GenericIdentity(username, "Custom");

                    // get the user roles
                    var roles = SecurityService.GetRoles(client.Name);

                    // set the custom principal
                    evaluationContext.Properties["Principal"] = new CustomPrincipal(client, roles);
                }
                else
                {
                    throw new TholosException("Current context cannot be null.");
                }

                return true;
            }, evaluationContext, state);
        }

        /// <summary>
        /// <para>
        /// Checks whether this instance was properly configured.
        /// </para>
        /// </summary>
        ///
        /// <exception cref="ConfigurationException ">
        /// If it is not configured.
        /// </exception>
        public void CheckConfiguration()
        {
            Helper.CheckConfiguration(SecurityService, "SecurityService");
            Helper.CheckConfiguration(Logger, "Logger");
        }
    }
}

