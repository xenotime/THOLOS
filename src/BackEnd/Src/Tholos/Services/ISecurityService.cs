/*
*  Copyright (c) 2015, TopCoder, Inc. All rights reserved.
*/

using System;
using System.Collections.Generic;
using System.ServiceModel.Web;

namespace Tholos.Services
{
    /// <summary>
    /// <para>
    /// This service is used to define the contract for the security operations.
    /// </para>
    /// </summary>
    /// 
    /// <threadsafety>
    /// Implementations of this interface should be effectively thread safe.
    /// </threadsafety>
    /// 
    /// <author>veshu</author>
    /// 
    /// <version>1.0</version>
    /// 
    /// <copyright>Copyright (c) 2015, TopCoder, Inc. All rights reserved.</copyright>
    public interface ISecurityService
    {
        /// <summary>
        /// Get roles. It will check for both - Manager and Participant.
        /// </summary>
        ///
        /// <param name="username">The username.</param>
        ///
        /// <returns>The roles.</returns>
        ///
        /// <exception cref="WebFaultException{T}">
        /// If any error occurred <see cref="ServiceFaultDetail"/> will be created from:
        /// <para>
        /// <list type="bullet">
        /// <item><see cref="ArgumentNullException"/> If <paramref name="username"/> is <c>null</c>.</item>
        /// <item><see cref="ArgumentException"/> If <paramref name="username"/> is empty.</item>
        /// <item><see cref="PersistenceException"/> If a DB-based error occurs.</item>
        /// <item><see cref="TholosException"/> If any other errors occur
        /// while performing this operation.</item>
        /// </list>
        /// </para>
        /// </exception>
        IList<string> GetRoles(string username);
    }
}

