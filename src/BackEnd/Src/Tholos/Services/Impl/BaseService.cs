/*
*  Copyright (c) 2015, TopCoder, Inc. All rights reserved.
*/

using log4net;
using Microsoft.Practices.Unity;

namespace Tholos.Services.Impl
{
    /// <summary>
    /// <para>
    /// This is the base class for all service implementations.
    /// </para>
    /// </summary>
    ///
    /// <threadsafety>
    /// <para>
    /// This class is mmutable but effectively thread-safe.
    /// </para>
    /// </threadsafety>
    /// 
    /// <author>veshu</author>
    ///
    /// <version>1.0</version>
    /// <copyright>Copyright (c) 2015, TopCoder, Inc. All rights reserved.</copyright>
    public abstract class BaseService
    {
        /// <summary>
        /// <para>
        /// Gets or sets the logger. It's used by subclasses to perform logging.
        /// </para>
        /// </summary>
        /// <value>
        /// The logger. It is not null after injection through Unity.
        /// </value>
        [Dependency]
        public ILog Logger
        {
            get;
            set;
        }

        /// <summary>
        /// <para>
        /// Initializes a new instance of the <see cref="BaseService"/> class.
        /// </para>
        /// </summary>
        protected BaseService()
        {
        }

        /// <summary>
        /// <para>
        /// Checks whether this instance was properly configured.
        /// </para>
        /// </summary>
        ///
        /// <exception cref="ConfigurationException ">
        /// If there are required injection fields that are not injected.
        /// </exception>
        public virtual void CheckConfiguration()
        {
            Helper.CheckConfiguration(Logger, "Logger");
        }


    }
}

