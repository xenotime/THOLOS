/*
*  Copyright (c) 2015, TopCoder, Inc. All rights reserved.
*/

using System.Runtime.Serialization;

namespace Tholos.Services
{
    /// <summary>
    /// This class represents a service fault details used for WCF RESTful error communication.
    /// </summary>
    ///
    /// <threadsafety>
    /// This class is mutable and not thread safe.
    /// </threadsafety>
    ///
    /// <author>veshu</author>
    ///
    /// <version>1.0</version>
    ///
    /// <copyright>Copyright (c) 2015, TopCoder, Inc. All rights reserved.</copyright>
    [DataContract]
    public class ServiceFaultDetail
    {
        /// <summary>
        /// Gets or sets the type of the error.
        ///  </summary>
        /// <value>
        /// The type of the error.
        /// </value>
        [DataMember]
        public string ErrorType
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the error message.
        /// </summary>
        /// <value>
        /// The error message.
        /// </value>
        [DataMember]
        public string ErrorMessage { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceFaultDetail"/> class.
        /// </summary>
        public ServiceFaultDetail()
        {
        }
    }
}

