'use strict';

/*
 *  Copyright (c) 2015, TopCoder, Inc. All rights reserved.
 */
/**
 * This file defines the common services for this application.
 * @author duxiaoyang
 * @version 1.0
 */
var appServices = angular.module('tholos.services', []);

// common services
appServices.factory('common', ['$http', '$q', function($http, $q) {
	return {
		/**
		 * Set credentials to default HTTP header.
		 * @param {String} the user name.
		 * @param {String} the password.
		 * @returns {String} the authentication header.
		 */
		setCredentials: function(username, password) {
			var authHeader = btoa(username + ':' + password);
			$http.defaults.headers.common.Authorization = 'Basic ' + authHeader;
			return authHeader;
		},
		/**
		 * Set the authentication header.
		 * @param {String} the authentication header.
		 */
		setAuthHeader: function(authHeader) {
			$http.defaults.headers.common.Authorization = 'Basic ' + authHeader;
		},
		/**
		 * Make an http request and add access token.
		 * @param {Object} options the options for $http call.
		 * @returns {Promise} promise.
		 */
		makeRequest: function(options) {
			var deferred = $q.defer();
			$http(options).success(function(data, status, headers, config) {
				deferred.resolve(data);
			}).error(function(data, status, headers, config) {
				deferred.reject(data);
			});
			return deferred.promise;
		}
	}
}]);